import {SET_BANANAS, SET_APPLES, SET_BANANAS_SUCCESS, SET_BANANAS_REQUEST} from '../actions/constants';


const initialState = {
  apples: 1,
  bananas: 3,
  isFetching: false
};

export const fruitsReducer = (state = initialState, action) => {
  switch (action.type) {
    case (SET_APPLES) :
      return {
        ...state,
        apples: action.payload
      };
    case (SET_BANANAS) :
      return {
        ...state,
        bananas: action.payload
      };
    case(SET_BANANAS_REQUEST) :
        return  {
          ...state,
          isFetching: true
      };

    case (SET_BANANAS_SUCCESS):
      return  {
        ...state,
        bananas: action.payload,
        isFetching: false
      };
    default :
      return state
  }
};

export const filmsReducer = (state = {movies: []}, action) => {
  switch (action.type) {
    case('GET_MOVIE_REQUEST') :
      return {
        ...state,
        isMovieFetching: action.isMovieFetching
      }
    case('GET_MOVIE_SUCCESS') :
      return {
        ...state,
        isMovieFetching: action.isMovieFetching,
        movies: action.payload
      }
    default : {
      return {
        ...state
      }
    }

  }
}